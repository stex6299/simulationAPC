//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file RunAction.cc
/// \brief Implementation of the B1::RunAction class

#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"
#include "G4AnalysisManager.hh"

#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//namespace B1
namespace { G4Mutex stuffMutex = G4MUTEX_INITIALIZER; }
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction()
    : G4UserRunAction()
{
    //using analysis manager for output
    auto analysisManager = G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run* run)
{
    G4RunManager::GetRunManager()->SetPrintProgress(run->GetNumberOfEventToBeProcessed()/100);

    // inform the runManager to save random number seed
    G4RunManager::GetRunManager()->SetRandomNumberStore(false);

    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------Begin of Global Run-----------------------";
    }
    else {
        G4int ithread = G4Threading::G4GetThreadId();

        G4cout
         << G4endl
         << "--------------------Begin of Local Run------------------------"
         << G4endl;
    }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run* run)
{

    G4int nofEvents = run->GetNumberOfEvent();

    // Run conditions
    //  note: There is no primary generator action object for "master"
    //        run manager for multi-threaded mode.

    // Print
    //
    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------End of Global Run-----------------------";
    }
    else {

      G4cout
       << G4endl
       << "--------------------End of Local Run------------------------";



      static bool first = true;
      stuffMutex.lock();
      std::ofstream file1;
      std::ofstream file2;
      std::ofstream file3;
      std::ofstream file4;
	  
      std::ofstream filetot;
      std::ofstream resAscii;

      if (first)
      {
          file1.open("results.dat");
          file2.open("Edep_calorimeter.dat");
          file3.open("Edep_S1.dat");
          file4.open("Edep_S2.dat");
		  
		  filetot.open("countGamma.dat");
		  resAscii.open("ASCII.dat");
          first = false;
		  
		  resAscii<<"Rino\tAPC1\tAPC2\tnumGammainAPC1"<<std::endl;

      }
      else
      {
          file1.open("results.dat", std::ios_base::app);
          file2.open("Edep_calorimeter.dat", std::ios_base::app);
          file3.open("Edep_S1.dat", std::ios_base::app);
          file4.open("Edep_S2.dat", std::ios_base::app);
		  
		  filetot.open("countGamma.dat", std::ios_base::app);
		  resAscii.open("ASCII.dat", std::ios_base::app);
      }
	  
      file1<<data0<<std::flush;
      for (int i = 0; i < Edep_all.size(); i++)
      {
          file2<<Edep_all[i]<<std::endl;
      }
      for (int i = 0; i < Edep_allS1.size(); i++)
      {
          file3<<Edep_allS1[i]<<std::endl;
      }
      for (int i = 0; i < Edep_allS2.size(); i++)
      {
          file4<<Edep_allS2[i]<<std::endl;
      }
      for (int i = 0; i < nGammaS1.size(); i++)
      {
          filetot<<nGammaS1[i]<<std::endl;
      }
	  
	  // Scrivo un unico ascii, con la speranza che tutti i vettori 
	  // abbiano la stessa lunghezza. Ma per costruzione è così
      for (int i = 0; i < nGammaS1.size(); i++)
      {
          resAscii<<Edep_all[i]<<"\t"<<Edep_allS1[i]<<"\t"<<Edep_allS2[i]<<"\t"<<nGammaS1[i]<<std::endl;

      }
	  
	  
	  
      file1.close();
      file2.close();
      file3.close();
      file4.close();
	  
	  filetot.close();
	  resAscii.close();
      stuffMutex.unlock();

    }

//    static G4Mutex stuffMutex = G4MUTEX_INITIALIZER;

    //stuffMutex.lock();

 // stuffMutex.unlock();

    G4cout
       << G4endl
       << " The run consists of " << nofEvents << " particles"
       << G4endl
       << "------------------------------------------------------------"
       << G4endl
       << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}
