//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the B1::DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4RegionStore.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"


// Color
#include "G4VisAttributes.hh"
#include "G4Colour.hh"


//namespace B1
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // Get nist material manager
    G4NistManager* nist = G4NistManager::Instance();

    G4Material* Silicon = nist->FindOrBuildMaterial("G4_Si");
    G4Material* Copper= nist->FindOrBuildMaterial("G4_Cu");
    G4Material* Plastic= nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
	
	//Unused materials
    //G4Material* Iridium = nist->FindOrBuildMaterial("G4_Ir");
    //G4Material* Tungsten = nist->FindOrBuildMaterial("G4_W");

    G4double density;
    G4int   ncomponents;
    G4int natoms;
    // This function illustrates the possible ways to define materials using
    // G4 database on G4Elements
    G4Material* PbO = new G4Material("PbO", density=  9.530 *g/cm3, ncomponents=2);
    G4Element* O  = nist->FindOrBuildElement(8);
    PbO->AddElement(O , natoms=1);
    G4Element* Pb = nist->FindOrBuildElement(82);
    PbO->AddElement(Pb, natoms= 1);

    G4bool checkOverlaps = true;

    G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
	
	
	
	
	
	
	// Ste - CsI crystal
	G4double A, Z;
	
	G4Element* elI  = new G4Element("Iodine",  "I",  Z=53., A= 126.90447*g/mole);
	G4Element* elCs = new G4Element("Cesium",  "Cs", Z=55., A= 132.90543*g/mole);

	density = 4.51 *g/cm3;
	G4Material* CsI = new G4Material("CsI", density, ncomponents= 2);
	
	CsI-> AddElement(elCs, natoms=1);
	CsI-> AddElement(elI,  natoms=1);
	
	// Ste - NaI crystal
	G4Element* elNa = new G4Element("Sodium",  "Na", Z=11., A= 22.989768*g/mole);
	
	density = 3.67 *g/cm3;
	G4Material* NaI = new G4Material("NaI", density, ncomponents= 2);
	
	NaI-> AddElement(elNa, natoms=1);
	NaI-> AddElement(elI,  natoms=1);
	
	
		
		
		

    //-- WORLD
    G4Box* solidWorld = new G4Box("World",10*m,10*m,50*m);
    G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
    G4VPhysicalVolume* physWorld = new G4PVPlacement
                        (0,                    // no rotation
                        G4ThreeVector(),       // centre position
                        logicWorld,            // its logical volume
                        "World",               // its name
                        0,                     // its mother volume
                        false,                 // no boolean operation
                        0,                     // copy number
                        checkOverlaps);        // overlaps checking
						
						
	// Il mondo deve essere nascosto, sennò non vedo dentro
	G4VisAttributes* worldColor= new G4VisAttributes(G4Colour(1.,1.,1.));
	worldColor->SetVisibility(false);
	logicWorld->SetVisAttributes(worldColor);


//-----------------------------------------------------------------------------------------------------------------
    //particle detector volume - First BC
    G4Box* Detector = new G4Box("C1",10*cm/2, 10*cm/2, 410*um/2);
    G4LogicalVolume* fLogicDetector = new G4LogicalVolume(Detector, Silicon,"C1");
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0.*m) , fLogicDetector, "C1", logicWorld, false, 0, checkOverlaps);
    //add this volume to vector for scoring at SteppingAction
    fScoringVolume.push_back(fLogicDetector);
	


//-----------------------------------------------------------------------------------------------------------------
    //particle detector volume - Second BC
    G4Box* Detector2 = new G4Box("C2",10*cm/2, 10*cm/2, 410*um/2);
    G4LogicalVolume* fLogicDetector2 = new G4LogicalVolume(Detector2, Silicon,"C2");
    new G4PVPlacement(0, G4ThreeVector(0, 0, 66*cm) , fLogicDetector2, "C2", logicWorld, false, 0, checkOverlaps);
    //add this volume to vector for scoring at SteppingAction
    fScoringVolume.push_back(fLogicDetector2);
	
		
//------------------------------------------------------------------------------------------------------------
	//Rame per Creare i fotoni, subito dopo la prima camera
	// Spessore in unità di X0
	G4double thicknessConvRame = 0.2 * Copper->GetRadlen();

	G4Box* Rame = new G4Box("Rame",10*cm/2, 10*cm/2, thicknessConvRame/2);
	G4LogicalVolume* fLogicRame = new G4LogicalVolume(Rame, Copper,"Rame");
	//new G4PVPlacement(0, G4ThreeVector(0., 0, 3.21*m) , fLogicRame, "Rame", logicWorld, false, 0, checkOverlaps);
	new G4PVPlacement(0, G4ThreeVector(0., 0, 66*cm + 410*um/2 + thicknessConvRame/2) , fLogicRame, "Rame", logicWorld, false, 0, checkOverlaps);
	//add this volume to vector for scoring at SteppingAction
	fScoringVolume.push_back(fLogicRame);



	

			// // -----------------------------------------------------------------------------------------------------------------
				// // Crystal parameters
				// G4double CrystalLengthX = 13*mm;//to check!!!
				// G4double CrystalLengthY = 15*mm;//to check!!!
				// G4double CrystalLengthZ = 2*mm;

				// // Setting crystal rotation angle (also the angle of crystal planes vs the beam)
				// G4double AngleX = 0;
				// G4RotationMatrix *xRot = new G4RotationMatrix;
				// xRot->rotateY(-AngleX);

				// // Setting crystal position
				// G4ThreeVector posCrystal = G4ThreeVector(0.*mm,0.*mm,3.81*m);

				// // setting crystal material
				// G4Material* CrystalMaterial = Tungsten;

				// // crystal volume
				// G4Box* crystalSolid = new G4Box("Crystal",CrystalLengthX/2,CrystalLengthY/2,CrystalLengthZ/2.);
				// G4LogicalVolume* crystalLogic = new G4LogicalVolume(crystalSolid,CrystalMaterial,"Crystal");
				// CrystalN1 = new G4PVPlacement(xRot,posCrystal,crystalLogic,"Crystal",logicWorld,false,0);;
				// // add this volume to vector for scoring at SteppingAction
				// fScoringVolume.push_back(crystalLogic);

			// // ------------------------------------------------------------------------------------------------------------
				// // Plastic0
				// G4Box* Plastic0 = new G4Box("Plastic",10*cm/2, 10*cm/2, 4*mm/2);// to check!!! all the geometry, especially thickness (recalculated due to wrong material taking into account the radiation lenght)
				// G4LogicalVolume* fLogicPlastic0 = new G4LogicalVolume(Plastic0, Plastic,"Plastic");// to check!!! material
				// new G4PVPlacement(0, G4ThreeVector(0*cm, 0, posCrystal.z()+5*mm) , fLogicPlastic0, "Plastic", logicWorld, false, 0, checkOverlaps);
				// // add this volume to vector for scoring at SteppingAction
				// fScoringVolume.push_back(fLogicPlastic0);

//------------------------------------------------------------------------------------------------------------
    //-- BENDING MAGNET (a cylinder)
    G4Box* Magnet = new G4Box("Magnet",40*cm/2, 40*cm/2, 40.*cm/2);
    G4LogicalVolume* logicMagnet = new G4LogicalVolume(Magnet,world_mat,"Magnet");
    new G4PVPlacement(0,G4ThreeVector(0, 0, 66*cm + 61*cm + 95*cm+ 40*cm/2),logicMagnet,"Magnet",logicWorld,false,0,checkOverlaps);

//------------------------------------------------------------------------------------------------------------
    //Scintillator1
    G4Box* Scintillator1 = new G4Box("S1",10*cm/2, 10*cm/2, 1*cm/2);// to check!!!
    G4LogicalVolume* fLogicScintillator1 = new G4LogicalVolume(Scintillator1, Plastic,"S1");
    new G4PVPlacement(0, G4ThreeVector(0*cm, 0, 8.87*m + 95*cm - 11.5*cm) , fLogicScintillator1, "S1", logicWorld, false, 0, checkOverlaps);
    //add this volume to vector for scoring at SteppingAction
    fScoringVolume.push_back(fLogicScintillator1);

//------------------------------------------------------------------------------------------------------------
   //Preshower
   // Spessore in unità di X0
   G4double thicknessConv = 0.3 * Copper->GetRadlen();
   
   G4Box* Preshower = new G4Box("Cu",10*cm/2, 10*cm/2, thicknessConv/2);
   G4LogicalVolume* fLogicPreshower = new G4LogicalVolume(Preshower, Copper,"Cu");
   new G4PVPlacement(0, G4ThreeVector(0., 0, 8.87*m + 95*cm - 3*cm - thicknessConv/2) , fLogicPreshower, "Cu", logicWorld, false, 0, checkOverlaps);
   //add this volume to vector for scoring at SteppingAction
   fScoringVolume.push_back(fLogicPreshower);

//------------------------------------------------------------------------------------------------------------
    //Scintillator2
    G4Box* Scintillator2 = new G4Box("S2",10*cm/2, 10*cm/2, 1*cm/2);// to check!!!
    G4LogicalVolume* fLogicScintillator2 = new G4LogicalVolume(Scintillator2, Plastic,"S2");
	new G4PVPlacement(0, G4ThreeVector(0*cm, 0, 8.87*m + 95*cm - 1.5*cm) , fLogicScintillator2, "S2", logicWorld, false, 0, checkOverlaps);
    //add this volume to vector for scoring at SteppingAction
    fScoringVolume.push_back(fLogicScintillator2);

//------------------------------------------------------------------------------------------------------------
    // //Calorimeter
    // G4Box* Calorimeter = new G4Box("Calorimeter",10*cm/2, 10*cm/2, 50.*cm/2);//10*10 cm2 is correct!!!
    // G4LogicalVolume* fLogicCalorimeter = new G4LogicalVolume(Calorimeter, PbO,"Calorimeter");
    // new G4PVPlacement(0, G4ThreeVector(0, 0, 12.26*m) , fLogicCalorimeter, "Calorimeter", logicWorld, false, 0, checkOverlaps); // Edited, chechare il resto
    // //add this volume to vector for scoring at SteppingAction
    // fScoringVolume.push_back(fLogicCalorimeter);
	
	
    //Calorimeter
    G4Box* Rino = new G4Box("Rino",23.5*cm/2, 23.5*cm/2, 39*cm/2);
    G4LogicalVolume* fLogicRino = new G4LogicalVolume(Rino, NaI,"Rino");
    new G4PVPlacement(0, G4ThreeVector(0, 0, 8.87*m + 95*cm + 39*cm/2) , fLogicRino, "Rino", logicWorld, false, 0, checkOverlaps); 
    //add this volume to vector for scoring at SteppingAction
    fScoringVolume.push_back(fLogicRino);
	
	
	
	
	
	
	
	
	// Colori dei vari pezzi

	
	// Scinti = blu
	G4VisAttributes* scintiColor= new G4VisAttributes(G4Colour(0.,0.,1., 1.));
	scintiColor->SetVisibility(true);
	scintiColor->SetForceSolid(true);
	fLogicScintillator1->SetVisAttributes(scintiColor);
	fLogicScintillator2->SetVisAttributes(scintiColor);


	// Silici = Verdi
	G4VisAttributes* siliColor= new G4VisAttributes(G4Colour(0.,1.,0., 1.));
	siliColor->SetVisibility(true);
	siliColor->SetForceSolid(true);
	fLogicDetector->SetVisAttributes(siliColor);
	fLogicDetector2->SetVisAttributes(siliColor);
	

	
	// Rame = Rosso
	G4VisAttributes* absColor= new G4VisAttributes(G4Colour(1.,0.,0., 1.));
	absColor->SetVisibility(true);
	absColor->SetForceSolid(true);
	fLogicPreshower->SetVisAttributes(absColor);
	fLogicRame->SetVisAttributes(absColor);
	
	
	// Magnet+calo = yellow
	G4VisAttributes* magColor= new G4VisAttributes(G4Colour(1.,1.,0., .5));
	magColor->SetVisibility(true);
	magColor->SetForceSolid(true);
	logicMagnet->SetVisAttributes(magColor);
	fLogicRino->SetVisAttributes(magColor);
	
	
	

	


	


	
	

  //always return the physical World
  return physWorld;
}

void DetectorConstruction::ConstructSDandField()
{
    // =============================
    //       MAGNETIC FIELD
    // =============================
    G4double fieldValue = 0.5/0.4*tesla;//not correct //to check!!!
    G4UniformMagField* myField = new G4UniformMagField(G4ThreeVector(0., fieldValue, 0.));
    G4FieldManager* fieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
    G4LogicalVolume* logicBox1 = G4LogicalVolumeStore::GetInstance()->GetVolume("Magnet");
    G4FieldManager* localfieldMgr = new G4FieldManager(myField);
    logicBox1->SetFieldManager(localfieldMgr,true);
    fieldMgr->CreateChordFinder(myField);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}
