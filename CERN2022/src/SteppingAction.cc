//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SteppingAction.cc
/// \brief Implementation of the B1::SteppingAction class

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"

#include "G4AnalysisManager.hh"

//namespace B1
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* eventAction)
: fEventAction(eventAction)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
    if (fScoringVolume.size()==0) {
      const DetectorConstruction* detConstruction
        = static_cast<const DetectorConstruction*>
          (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
      fScoringVolume = detConstruction->GetScoringVolume();
    }

    G4LogicalVolume* volume
        = step->GetPreStepPoint()->GetTouchableHandle()
          ->GetVolume()->GetLogicalVolume();
    G4ThreeVector MomentumDirection;
    G4int VolumeNumber = fScoringVolume.size();



    // // //if volume enter
    // // if (step->GetPostStepPoint()-> GetStepStatus()==G4StepStatus::fGeomBoundary)
    // // {
        // // for (int i = 0; i < VolumeNumber-1; i++)
        // // {
            // // if(volume==fScoringVolume[i])
            // // {
                // // //writing data of each volume from the list fScoringVolume
                // // std::ostringstream ss_z;
                // // ss_z.setf(std::ios::scientific);
                // // ss_z<<volume->GetName()<<" "<<std::flush;
                // // ss_z<<step->GetTrack()->GetTrackID()<<" "<<std::flush;//particle ID
                // // ss_z<<step->GetTrack()->GetDefinition()->GetParticleName()<<" "<<std::flush;//particle name
                // // ss_z<<step->GetPostStepPoint()->GetPosition().getX()/mm<<" "<<std::flush; //coordinate x
                // // ss_z<<step->GetPostStepPoint()->GetPosition().getY()/mm<<" "<<std::flush; //coordinate y
                // // ss_z<<step->GetPostStepPoint()->GetPosition().getZ()/mm<<" "<<std::flush; //coordinate z

                // // MomentumDirection = step->GetPostStepPoint()->GetMomentumDirection();
                // // ss_z<<atan(MomentumDirection.getX()/MomentumDirection.getZ())<<" "<<std::flush; //horizontal angle
                // // ss_z<<atan(MomentumDirection.getY()/MomentumDirection.getZ())<<" "<<std::flush; //vertical angle
                // // ss_z<<step->GetPostStepPoint()->GetKineticEnergy()/MeV<<G4endl; //kinetic energy

                // // fEventAction->AddOutputVolumeBoundary(ss_z.str());

            // // }
        // // }
    // // }


	// Riempimento energie integrate per singolo detector
    if(volume->GetName()=="S1")
    {
        fEventAction->AddDepositedEnergyS1(step->GetTotalEnergyDeposit()/MeV);
		
		
		// === Provo a contare i fotoni che entrano in S1 ===
		
		// Se entra nel primo scintillatore
		if (step->GetPreStepPoint()-> GetStepStatus()==G4StepStatus::fGeomBoundary)
		{
			// Se è un fotone
			if (step->GetTrack()->GetDefinition()->GetParticleName()=="gamma")
			{
				// Se è stato creato dal primo elettrone
				if (step->GetTrack()->GetParentID() == 1)
				{
					std::ostringstream ss_z;
					ss_z.setf(std::ios::scientific);
					
					ss_z<<volume->GetName()<<" "<<std::flush;										// Volume name
					ss_z<<step->GetTrack()->GetTrackID()<<" "<<std::flush; 							// Particle ID
					ss_z<<step->GetTrack()->GetParentID()<<" "<<std::flush; 						// Parent ID
					ss_z<<step->GetTrack()->GetDefinition()->GetParticleName()<<" "<<std::flush;	//particle name
					ss_z<<G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID()<<" "<<std::flush;		//event number
					ss_z<<step->GetPreStepPoint()->GetKineticEnergy()/MeV<<G4endl; 					// kinetic energy

					fEventAction->AddOutputVolumeBoundary(ss_z.str());
					
					
					// Lo conto
					
					fEventAction->countGamma();	// Lo conto

					
				}
			}
			
		}

    }

    if(volume->GetName()=="S2")
    {
        fEventAction->AddDepositedEnergyS2(step->GetTotalEnergyDeposit()/MeV);
    }

    if(volume->GetName()=="Rino")
    {
        fEventAction->AddDepositedEnergy(step->GetTotalEnergyDeposit()/MeV);
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}
