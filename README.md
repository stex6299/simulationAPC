# What's here
Per ora ho messo la simulazione che mi ha gentilmente fornito il nostro caro [Alex](mailto:sytov@fe.infn.it).

# What to do
Quello che serve a noi, citando una mail di Michela

> Caro Stefano, 
> nell'ordine: 
> *	setup sperimentale come hai indicato tu. Fascio di elettroni di dimensione quanto il trigger (in realtà era una striscia perché abbiamo tagliato solo in y nella prima settimana) e di divergenza come misurata. Sia con la targetta di rame che senza visto che abbiamo anche misurato il fondo. No cristallo: intanto riproduciamo i dati sperimentali "facili". Ricordati che ogni silicio sono 410 um di spessore
> *	per le quantità da salvare: 
>   *	coordinate sia spaziali che angolari di emissione di ogni fotone in ciascuno dei materiali e energia
>   *	segnale in ciascuno degli scintillatori 
>   *	segnale nel calorimetro
> Quindi per ogni evento avrai 1 o più fotoni emessi di cui bisogna conservare la storia. Nel calorimetro ci sarà l'energia totale. Negli scintillatori ci sarà l'energia totale dei fotoni che hanno convertito ma avendo la storia di tutti i fotoni prodotti nell'evento possiamo sapere quanti sono e chi ha convertito. 
> Dimmi se non è chiaro
> 
> Michela







# Install Geant4 on WSL2 (I have Ubuntu 20.04)

## (Install and) verify you have WSL2 (and not 1)
[This](https://winaero.com/update-from-wsl-to-wsl-2-in-windows-10/#:~:text=To%20update%20from%20WSL%20to%20WSL%202%20in%20Windows%2010%2C&text=Restart%20Windows%2010.,set%2Ddefault%2Dversion%202%20.) is the reference guide. Shortly

1. Enable *Windows Subsystem for Linux* and *Virtual Machine platform* via Power Shell (run as admin). It can be done also via control panel, under Programs and features -> the last item. If you already have a WSL, you can skip this step
```powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```
If you do it, you need to restart the system

2. Download and install the latest Linux kernel update package (Step 5 of the guide)

3. Set WSL 2 as your default version (It's optional. But this way any new distro installed is configured as WSL 2)
```powershell
wsl --set-default-version 2
```
Now every distro you will install is as WSL2

4. Change your existing WSL to WSL2. First get the name of your distro and, if the version is 1, set it to 2 (First command check version, second eventually to set it)
```powershell
wsl -l -v
wsl --set-version Ubuntu 2
```
where `Ubuntu` is the name of my distro

### Allowing WSL2 to output graphics
1. First of all, VcXsrv in necessary [Download here](https://sourceforge.net/projects/vcxsrv/)
2. Follow [this link](https://stackoverflow.com/questions/61110603/how-to-set-up-working-x11-forwarding-on-wsl2). Basically you have to add in `~/.bashrc`
```
export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
export LIBGL_ALWAYS_INDIRECT=1
```
And when startinx VcXsrv (you will find in start menu under `XLaunch`, you have to: Multiple windows > Start no client > Uncheck "Native opengl" and check "Disable access control"



### Alcuni comandi appoggiati qua
```powershell
systeminfo | find "System Type"
wsl.exe --list --all
wsl --set-default-version 2
wsl --set-version Ubuntu 2
wsl -l -v
wsl --update
wsl --shutdown
```





## Install CMake 
```bash
sudo apt purge cmake
sudo apt install libssl-dev

mkdir CMake && cd CMake
wget https://github.com/Kitware/CMake/releases/download/v3.18.2/cmake-3.18.2.tar.gz
sudo tar xzf cmake-3.18.2.tar.gz && cd cmake-3.18.2

sudo ./bootstrap
sudo make -j4
sudo make install
```

## Install some dependencies
If the second command results in an error, you should try with `sudo apt update` and then `sudo apt -y install libglu1-mesa-dev freeglut3-dev mesa-common-dev xorg --fix-missing`
```bash
sudo apt -y install build-essential
sudo apt -y install libglu1-mesa-dev freeglut3-dev mesa-common-dev xorg
sudo apt -y install qtcreator qt5-default
sudo add-apt-repository ppa:rock-core/qt4
sudo apt update
sudo apt -y install libqt4-declarative qt4-dev-tools qt4-qmake libqtwebkit4
sudo apt -y install libxmu-dev libxerces-c-dev libexpat1-dev libssl-dev
sudo apt -y update && sudo apt -y upgrade
```


## Install Geant4 11.0.2
Compatible with Alex's simulations. First of all return in the main directory
```bash
cd
```
and then
```bash
mkdir Geant4.11.0.2 && cd Geant4.11.0.2

wget https://github.com/Geant4/geant4/archive/refs/tags/v11.0.2.tar.gz && tar -xvf v11.0.2.tar.gz

mkdir geant4-11.0.2-build && cd geant4-11.0.2-build

cmake -DCMAKE_INSTALL_PREFIX=~/Geant4.11.0.2/geant4-11.0.2-install ~/Geant4.11.0.2/geant4-11.0.2  -DGEANT4_USE_OPENGL_X11=ON -DGEANT4_USE_QT=ON -DGEANT_BUILD_MULTITHREADED=ON -DGEANT4_INSTALL_DATA=ON
make -j8
make install
```


# How to use Geant4 on a VM
The trick is that you don't have to install!!! You can simpli source from CVMFS. All this commands are reported in [comandi.txt](./comandi.txt). It's a good idea when opening the folder `cat comandi.txt`

Let's source gcc and Geant4
```bash
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/9/x86_64-centos8/setup.sh
source  /cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/GNUMake-setup.sh
```
Eventualmente se servisse CMake
```bash
source /cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/setup.sh
```

From the build folder, you can then compile
```bash
cmake -DGeant4_DIR=/cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/lib64/Geant4-11.0.2/ ../CERN2022 && make -j8
```



# Run the simulation
Go in the [build folder](./CERN2022-build) and run to prepare and compile the simulation
```bash
# Local
cmake -DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/ ../CERN2022 && make -j4

# On VM
cmake -DGeant4_DIR=/cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/lib64/Geant4-11.0.2/ ../CERN2022 && make -j8
```

To run the simulation with a macro
```bash
./exampleB1 run1.mac
```

To open the GUI
```bash
./exampleB1
```

To create nice pics for presentation
1. Create a macro file (.mac) with `/run/beamOn 1`
2. Open the GUI `./exampleB1`
3. Run in the console `/control/execute run1.mac`
This way it shot one particle and shows the tracks



























## (Old but detailed) - Installation of Geant4.10.06
This version is compatible with Valerio and Mattia's simulations

1. Create a folder and enter it
```bash
mkdir Geant4.10.06 && cd Geant4.10.06
```

2. Download the desired version (format .tar.gz). You can find a complete list [here](https://github.com/Geant4/geant4/releases)
```bash
wget http://cern.ch/geant4-data/releases/geant4.10.06.p02.tar.gz
```
and then extract 
```bash
tar -xvf geant4.10.06.p02.tar.gz
```

3. Create a folder for the build and enter it
```bash
mkdir mkdir geant4.10.06.p02-build && cd mkdir geant4.10.06.p02-build
```

4. Run the following command
```bash
cmake -DCMAKE_INSTALL_PREFIX=~/Geant4.10.06/geant4.10.06.p02-install ~/Geant4.10.06/geant4.10.06.p02 -DGEANT4_USE_OPENGL_X11=ON -DGEANT4_USE_QT=ON -DGEANT_BUILD_MULTITHREADED=ON -DGEANT4_INSTALL_DATA=ON
```
and then
```bash
make -j8
```
where -j is the number of processes. 4 is reasonably. I use 8 because I am ingordo. Actually, if you don't need to use your PC, this speeds up the installation. It can lasts several minutes to a couple of hour

finally
```bash
make install
```


5. For setting up all the variables, a source should be performed every time you start up your machine
```bash
source ~/Geant4.10.06/geant4.10.06.p02-install/bin/geant4.sh
```

It is convenient to add the following lines in `~/.bashrc` file, a script which is executed each time you login
```
export G4INSTALL=~/Geant4.10.06/geant4.10.06.p02-install
source $G4INSTALL/bin/geant4.sh
```
Of course, you should put your exact path... Per eseguire al volo il contenuto del `~/.bashrc`: 
```bash
cd
. .bashrc
```










